# Dialogflow Connector - Custom Fulfillment


*Connector service to relay messages between Zoko and Dialogflow*


This demo purpose : respond to an incoming message sent to your whatsapp number (on Zoko) from a customer. For this purpose, the message 
is sent to this service end point using webhooks.

This connector will 

- read the incoming message and gets the message content 

- sends the content to dialogflow agent and gets the intent

- responds to the user using Zoko API with the response from DialogFlow


## Steps 

- update the `.env` file with relevant values (Zoko and Dialogflow credentials)

- `docker-compose build` (builds the image named `zoko-dialog`)

- `docker-compose up` (runs the docker container on port 8083. Change the port on docker-compose if needed)

- end point setup would be : `<hostname>/v1/bot/dialogflow`

- setup this as the webhook url on Zoko for incoming messages `message:user:in`


