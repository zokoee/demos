const Router = require("koa-router");
const messageHandler = require("../controllers/webhook");

const router = new Router();

router.post("/gig/bot/gsheet", messageHandler.gsheetHandler);
router.post("/gig/bot/webhookTest", messageHandler.webhookHandler);

module.exports = router;
