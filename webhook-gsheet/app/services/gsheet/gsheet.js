const { GoogleSpreadsheet } = require("google-spreadsheet");
const sendMessage = require("../zoko/sendMessage");

const creds = require("./gsheetnodedemo-cred.json");

exports.getGSheetData = async (requestBody) => {
  let senderWhatsAppNumber = requestBody.platformSenderId;
  let customerName = requestBody.customerName;

  sendMessage.sendTextMessage(
    senderWhatsAppNumber,
    `Hey ${customerName}, please wait while we are fetching your data...`
  );

  await sleep(2000);

  try {
    // spreadsheet key is the long id in the sheets URL
    const doc = new GoogleSpreadsheet(
      "15VgWiVfyBQVilQS999v39OZEaEWF5WdiB8-hLioih3k" // Spreadsheet Key
    );

    // use creds
    doc.useServiceAccountAuth(creds);

    await doc.loadInfo(); // loads document properties and worksheets

    console.log(`DOCUMENT TITLE: ${doc.title}`);

    // await doc.updateProperties({ title: "Amazon Merchant Data Test" });

    const sheet = doc.sheetsByIndex[0];
    console.log(`SHEET TITLE: ${sheet.title}`);
    console.log(`SHEET ROW COUNT: ${sheet.rowCount}`);

    // LOAD HEADER ROWS
    await sheet.loadHeaderRow();

    // const rows = await sheet.getRows({
    //   offset: 2,
    //   limit: 3,
    // });

    const rows = await sheet.getRows();

    const result = getFormattedString(
      rows,
      sheet.headerValues,
      senderWhatsAppNumber
    );
    sendMessage.sendTextMessage(senderWhatsAppNumber, result);
  } catch (err) {
    console.log(err);
  }
};

function getFormattedString(rows, headerRow, senderWhatsAppNumber) {
  let formattedString = "";
  rows.forEach((row) => {
    if (senderWhatsAppNumber.replace("+", "") === row["WhatsApp Number"]) {
      headerRow.forEach((headerValue) => {
        formattedString += `\n${headerValue}: ${row[headerValue]}`;
        //   console.log(`${headerValue}: ${row[headerValue]}`);
      });
      formattedString +=
        "\n\n------------------------------------------------------------\n";
      // console.log("--------------------");
    }
  });
  return formattedString.length
    ? formattedString
    : "Sorry. Couldn’t find your details. Please contact support team.";
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
