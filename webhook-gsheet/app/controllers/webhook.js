const moment = require("moment");
const gheetService = require("../services/gsheet/gsheet");
const response = require("../util/response");

exports.gsheetHandler = async (ctx) => {
  response.sendResponse(ctx, 200, {});

  gheetService.getGSheetData(ctx.request.body);
};

exports.webhookHandler = async (ctx) => {
  // console.log("Request - ", ctx.request.body);
  console.log(
    moment().toISOString() +
      " : Custome -> " +
      ctx.request.body.platformSenderId +
      "   Message: -> " +
      ctx.request.body.text
  );
  response.sendResponse(ctx, 200, "success");
};
