const express = require( 'express' );
const app = express();
app.use( express.json() );

app.post( '/incoming', ( req, res ) => {
    console.log( 'received incoming message', req.body );
    
    // send the response back ASAP
    res.sendStatus( 200 );

    // now process the message 
    console.log( 'ready to process message');

    // print the sender number and message
    console.log( 'message from - ', req.body.platformSenderId);
    console.log( 'message type - ', req.body.type);
    console.log( 'message text - ', req.body.text);
    console.log( 'message channel - ', req.body.platform);	
} );

app.post( '/outgoing', ( req, res ) => {
    console.log( 'received incoming message', req.body );

    // send the response back ASAP
    res.sendStatus( 200 );

    // now process the message
    console.log( 'ready to process message');

    // print the sender number and message
    console.log( 'message to - ', req.body.platformSenderId);
    console.log( 'message type - ', req.body.type);
    console.log( 'message text - ', req.body.text);
    console.log( 'message channel - ', req.body.platform);
} );


app.post( '/status', ( req, res ) => {
    console.log( 'received message delivery status update', req.body );

    // send the response back ASAP
    res.sendStatus( 200 );

    // now process the message
    console.log( 'ready to process message');

    // print the sender number and message
    console.log( 'message id -', req.body.id);
    console.log( 'message from - ', req.body.platformSenderId);
    console.log( 'message status - ', req.body.deliveryStatus);
    console.log( 'message channel - ', req.body.platform);
} );


app.listen( 9000, () => console.log( 'Node.js server started on port 9000.' ) );
